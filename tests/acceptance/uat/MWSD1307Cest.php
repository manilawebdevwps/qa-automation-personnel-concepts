<?php
namespace uat;
use \WebGuy;

class MWSD1307Cest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function ToggleActivationEmails(WebGuy $I) {
        $I->amOnPage('/admin');
        $I->wantTo('See toggling Emails Active/Inactive');
        $I->fillField('username','pcadmin');
        $I->fillField('password','pc@123');
        $I->click('//*[@id="frmlogin"]/fieldset/ul/li[3]/div/input[1]');
        $I->wait(4);
        $I->click('Email Templates');
        $I->checkOption('//*[@id="chk_all"]');
        $I->click('Edit');
        $I->canSee('Status');
    
    }

}