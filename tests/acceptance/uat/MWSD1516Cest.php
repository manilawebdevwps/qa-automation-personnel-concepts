<?php
namespace uat;
use \WebGuy;

class MWSD1516Cest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function TitlePage(WebGuy $I) {
        $I->wantToTest('Title page has Personnel Concepts at the end');
        $I->amOnPage('/labor-law-posters/');
        $I->canSeeElementInDOM('Labor Law Posters| Personnel Concepts');
        $I->expectTo('Every page has Personnel Concepts on Title');
    
    }

}