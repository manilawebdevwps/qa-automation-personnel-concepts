<?php
namespace uat;
use \AcceptanceTester;

class MWSD2606Cest
{
    // tests
    public function RRonSearchPage(AcceptanceTester $I)
    {
        $I->wantTo('Test searched term');
        $I->expectTo('See ');
        $I->amOnPage('/');
        $I->fillField("#searchBoxTF", "FD-SS2-L");
        $I->click('#frmProductSearch button');
        $I->seeInCurrentUrl('/search.php?search_text=FD-SS2-L');
        $I->see('Federal OSHA Safety Poster');
    }
}