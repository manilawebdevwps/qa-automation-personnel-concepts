<?php
namespace uat;
use \WebGuy;

class MWSD2090Cest
{

    public function _before()
    {
    }

    public function _after()
    {
    }


    // tests
    public function TitlePage(WebGuy $I) {
        $I->wantTo('Check custom 404 page for invalid pages');
        $I->expectTo('See 404 error page on invalid pages');
        $I->amOnPage('/labor-law-posters/dewdewd232');
        $I->seeElement('.content');
        $I->seeElement('.no-page-found-msg1');
        $I->seeElement('.no-page-found-msg2');
        $I->amOnPage('/osha-safety-posters/thisiserror12345');
        $I->seeElement('.content');
        $I->seeElement('.no-page-found-msg1');
        $I->seeElement('.no-page-found-msg2');
    }

}