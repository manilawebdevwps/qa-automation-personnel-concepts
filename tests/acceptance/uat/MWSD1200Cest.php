<?php
namespace uat;
use \WebGuy;

class MWSD1200Cest
{
    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function checkRobottext(WebGuy $I)
    {
        $I->am('SEO manager');
        $I->wantTo('Optimize robot text');
        $I->expectTo('see that 4 lines of codes are removed.');
        $I->amOnPage('/robots.txt');
        $I->waitForUserInput();
        $I->dontSee('/media/');
        $I->dontSee('/picture_library/');
        $I->dontSee('/gallery/');
        $I->dontSee(' /form_pdfs/');
    }
}