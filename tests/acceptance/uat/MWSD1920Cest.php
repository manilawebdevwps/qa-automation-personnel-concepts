<?php
namespace uat;
use \WebGuy;

class MWSD1465Cest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function Adlucent(WebGuy $I) {
        $I->wantToTest('Adlucent');
        $I->amOnPage('/fmla-compliance/fmla-medical-certification-form-for-employees/');
        $I->fillField('.product_txtbx', '1');
		$I->click('.addToCart');
		$I->amOnPage('/account-login/');
		$I->wait('10');
		$I->click('#rbtOld');
		$I->fillField('email', 'jericrealubit@yahoo.com');
		$I->fillField('cpassword', 'jeric123');
        $I->click('.secureLoginButton');
        $I->wait('10');
		$I->click('.chkoutBtn');
		$I->wait('10');
		$I->click('#UPC');
		$I->click('#invoiceme');
		$I->click('#termscondition');
		$I->wait('10');
		$I->click('#btn_submit_order');
		$I->wait('10');
		$I->seeInPageSource('var retailer = "personnelconcepts"');
        $I->expect('Adlucent code is embeded on the entire website.');
		$I->wait('20');
        /* $I->amOnPage('/search.php');
        $I->seeInPageSource("_gaq.push([_trackEvent', 'SiteSearch',");
        $I->expect('GA tracking event code is embeded on the entire website.'); */
    
    }

}