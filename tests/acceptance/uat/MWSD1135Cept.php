<?php
$I = new WebGuy($scenario);
$I->amOnPage('/');
$I->wantTo('Check Banner of the homepage');
$I->seeElement('//*[@id="contentGroup"]/div[2]/div[1]/div');
$I->expectTo('See banner of the homepage');
$I->click('.hp-banner');
$I->wait(10);
$I->canSeeCurrentUrlEquals('/labor-law-posters/state-federal-labor-law-posters/');
$I->expect('To load state federal labor law poster page');
?>