<?php
namespace uat;
use \WebGuy;

class MWSD1465Cest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function GA_search_event_tracking(WebGuy $I) {
        $I->wantToTest('Google Analytics Tracking event for search');
        $I->amOnPage('/');
        $I->fillField('search', 'labor law');
        $I->clickWithRightButton('//*[@id="frmProductSearch"]/button');
        $I->wait('30');
        $I->amOnPage('/search.php');
        $I->seeInPageSource("_gaq.push([_trackEvent', 'SiteSearch',");
        $I->expect('GA tracking event code is embeded on the entire website.');
    
    }

}