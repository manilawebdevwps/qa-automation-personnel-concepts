<?php
namespace uat;
use \WebGuy;

class MWSD1518Cest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function removeMetaKeywordHomePage(WebGuy $I) {
        $I->wantToTest('Meta keyword is removed on home page');
        $I->amOnPage('/');
        $I->dontSeeElementInDOM('<meta name="keywords" content="State and Federal Labor Law Posters, Compliance Posters, OSHA Safety Posters | Personnel Concepts" />
');
        $I->expectTo('Meta keyword is being removed on home page');
    }

    public function removeMetaKeywordInnerPage(WebGuy $I) {
        $I->wantToTest('Meta keyword is removed on home page');
        $I->amOnPage('/compliance-alerts/');
        $I->dontSeeElementInDOM('<meta name="keywords" content="State and Federal Labor Law Posters, Compliance Posters, OSHA Safety Posters | Personnel Concepts" />
');
        $I->expectTo('Meta keyword is being removed on innerpages');
    }

}