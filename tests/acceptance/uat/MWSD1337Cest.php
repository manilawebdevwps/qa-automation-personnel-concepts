<?php
namespace uat;
use \WebGuy;

class MWSD1337Cest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function TestCanonicalTag(WebGuy $I) {
        $I->wantToTest('the canonical tags in the CMS admin');
        $I->amOnPage('/compliance-alerts/');
        $I->expect('not append the /?item=... tag to the canonical URL');
        $I->canSeeElementInDOM('rel=canonical');
        $I->cantSeeElementInDOM('/?item=');
    }

}