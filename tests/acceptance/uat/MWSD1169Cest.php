<?php
namespace uat;
use \WebGuy;

class MWSD1169Cest
{

    public function _before()
    {
    }

    public function _after()
    {
    }


    public static $cart_qty= '.tabl_california > form:nth-child(1) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(4) > input:nth-child(1)';
    public static $cartElement_couponCode = '.proBox';

    // tests
    public function tryToTest(WebGuy $I) {
        $I->wantTo('want to clean-up promotional terminology');
        $I->expectTo('see promotional terminology change');
        //$I->changeBaseURL('http://208.69.127.5/personnelconcepts/');
        $I->amOnPage('labor-law-posters/alabama-labor-law-poster/');
        $I->wait(5);
        $I->fillField(self::$cart_qty,'1');
        $I->click('.addToCart');
        $I->seeCurrentUrlEquals('/cart/');
        $I->dontSee('Get Offer');
        $I->seeElement('.getoffersBtn');
        //$I->dontSee('Offer');
        $I->dontSee('Offer','.mcSubTotal5');
        //$I->dontSee('Enter Reference Number');
        //$I->dontSee('Enter Reference Number','td.mcSubTotal3:hover');
        $I->see('Enter Reference Number/Promo Code','.mcSubTotal3');
        //invalid code
        $I->fillField(self::$cartElement_couponCode,'123456');
        $I->click('.getoffersBtn');
        $I->dontSee('.message_error');
        $I->dontSee('Invalid key code','li.message_error');
        $I->see('is not a valid code');
        //valid keycode - W05849 and W05850
        $I->fillField(self::$cartElement_couponCode,'W05849');
        $I->click('.getoffersBtn');
        $I->dontSee('.message_success');
        $I->dontSee('was successfully applied','.message_success');
        $I->see('was successfully applied','.mcSubTotal5');
        $I->click('.mcMaintotal>td>a>img');
        $I->click('//img[contains(@src,"http://www.personnelconcepts.com/media/images/continue-chkout.jpg")]');

    }

}