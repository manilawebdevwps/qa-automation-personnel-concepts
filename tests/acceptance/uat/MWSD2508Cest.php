<?php
namespace uat;
use \WebGuy;

class MWSD2508Cest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function SeeCanonical(WebGuy $I) {
       //Header Canonical Tag
        $I->wantTo('To check Canonical Tag'); 
        $I->expectTo('See Canonical Tag'); 
        $I->amOnPage('http://personnelconcepts.zeondemo.com/labor-law-posters/utah-labor-law-poster/');
        $I->wait(5);
        $I->canSeeInPageSource('rel="canonical"');
        $I->canSeeInPageSource('href="http://personnelconcepts.zeondemo.com/labor-law-posters/utah-labor-law-poster/"');
        
        $I->amOnPage('http://personnelconcepts.zeondemo.com/labor-law-posters/washington-labor-law-poster/');
        $I->wait(5);
        $I->canSeeInPageSource('rel="canonical"');
        $I->canSeeInPageSource('href="http://personnelconcepts.zeondemo.com/labor-law-posters/washington-labor-law-poster/"');
        
        $I->amOnPage('http://personnelconcepts.zeondemo.com/labor-law-posters/montana-labor-law-poster/');
        $I->wait(5);
        $I->canSeeInPageSource('rel="canonical"');
        $I->canSeeInPageSource('href="http://personnelconcepts.zeondemo.com/labor-law-posters/montana-labor-law-poster/"');
        
        $I->amOnPage('http://personnelconcepts.zeondemo.com/labor-law-posters/new-york-labor-law-poster/');
        $I->wait(5);
        $I->canSeeInPageSource('rel="canonical"');
        $I->canSeeInPageSource('href="http://personnelconcepts.zeondemo.com/labor-law-posters/new-york-labor-law-poster/"');
        
        $I->amOnPage('http://personnelconcepts.zeondemo.com/labor-law-posters/louisiana-labor-law-poster/');
        $I->wait(5);
        $I->canSeeInPageSource('rel="canonical"');
        $I->canSeeInPageSource('href="http://personnelconcepts.zeondemo.com/labor-law-posters/louisiana-labor-law-poster/"');
        
        $I->amOnPage('http://personnelconcepts.zeondemo.com/labor-law-posters/massachusetts-labor-law-poster/');
        $I->wait(5);
        $I->canSeeInPageSource('rel="canonical"');
        $I->canSeeInPageSource('href="http://personnelconcepts.zeondemo.com/labor-law-posters/massachusetts-labor-law-poster"');
        
    }

}