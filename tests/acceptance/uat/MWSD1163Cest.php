<?php
namespace uat;
use \WebGuy;

class MWSD1163Cest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function checkNavigation(WebGuy $I) {
        $I->wantTo('Check Navigation');
        $I->maximizeWindow();
        $I->wait(4);
        $I->amOnPage('/');
        $I->expectTo('See Menu Containter');
        $I->canSeeElement('.main');
        $I->moveMouseOver('/html/body/div[2]/div[1]/div/ul/li[1]');
        $I->click('/html/body/div[2]/div[1]/div/ul/li[1]/ul/li[1]/a');
        $I->expectTo('see Level 2 Category on hover');
    }
    public function checkLink(WebGuy $I){
        $I->wantTo('Check Registration/Login links');
        $I->expectTo('See Login Link');
        $I->canSeeElement('.login');
        $I->expectTo('See Register Link');
        $I->canSeeElement('.register');
    }
    public function checkCartAmountTotal(WebGuy $I){
        $I->amOnPage('/');
        $I->wantTo('Check Cart Amount');
        $I->expectTo('See Cart Logo');
        $I->canSeeElement('.logo');
        $I->selectOption('state_url', 'Federal');
        $I->wait(4);
        $I->click('.hp-go-btn');
        $I->fillField('qty[FD-SS5-L]','1');
        $I->click('btnAddToCart');
        $I->waitForElementVisible('.chkOutTab');
        $I->amOnPage('/');
        $I->expectTo('See Cart Total Amount Computation');
        $I->canSeeElement('.price');
        $I->cantSee('$0.00','/html/body/header/div[2]/div[2]/div/span/strong');
    }
    public function checkInnerPage(WebGuy $I){
        $I->wantTo('Check Inner Page');
        $I->expectTo('See Inner Page Navigation');
        $I->canSeeElement('.top-nav');
        $I->expectTo('See Inner Forms');
        $I->canSeeElement('.content');
        $I->fillField('offercode','c19857');
        $I->click('/html/body/div[2]/div[3]/div[2]/div[1]/form/button');
        $I->waitForElement('.blueheading',3);
    }
    public function checkFeatureProducts(WebGuy $I){
        $I->amOnPage('/');
        $I->wantTo('check Feature Products');
        $I->expectTo('See Feature Products Container');
        $I->canSeeElement('./html/body/div[2]/div[5]');
    }
    public function checkHeaderFooter(WebGuy $I){
        $I->wantTo('Check Header and Footer');
        $I->expectTo('See Header Container');
        $I->canSeeElement('.header');
        $I->canSeeLink('Login','public/my-account');
        $I->canSeeLink('Register','public/register');
        $I->expectTo('See Footer Container');
        $I->canSeeElement('.footer');
    }

}