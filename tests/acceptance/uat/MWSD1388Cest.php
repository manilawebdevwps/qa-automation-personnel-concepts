<?php
namespace uat;
use \WebGuy;

class MWSD1388Cest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function SearchSpeed(WebGuy $I) {
    $I->amOnPage('/');
    $I->wantToTest('Search Result Relevance keyword');
    $I->expectTo('I want to see the Relevance');
    $I->fillField('Search[search]', 'labor law');
    $I->canSee('California Labor Law Poster');

}